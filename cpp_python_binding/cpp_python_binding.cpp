#include <pybind11/pybind11.h>


int test_module(int a, int b)
{
	return a + b;
}

namespace py = pybind11;

PYBIND11_MODULE(cpp_python_binding, m)
{
    m.def("test_module", &test_module, R"pbdoc(Test Module)pbdoc");
}
