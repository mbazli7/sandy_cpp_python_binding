# C++ Binding in Python Application
This is example project solution created in Visual Studio 2019 environment to bind C++ functions into Python application.

# Folder Structure
There is 2 projects in this solution,  
1. `cpp_python_binding` contains C++ functions and build using Visual Studio environment and,  
2. `python_app` contains Python application that call functions from `cpp_python_binding`.

# System Environment
Since our Python program is using x64 as target, thus `cpp_python_binding` is required to set its target into x64.  
Ensure that all `additional include libraries/path` has been set when change the target from win32 into x64.

# Dependencies
This example using `PyBind11` due to its simplicity and less code required to support our main objectives.  
Below are settings that need to be set in Visual Studio.  

| Tab              | Property                                | Value                                                                                                                     |
|------------------|-----------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| General          | Target Name                             | Specify name of the module to refer to it from Python in `from..import` statements                                        |
|                  | Configuration Type                      | Dynamic Library (.dll)                                                                                                    |
|                  | Advanced > Target File Extension        | .pyd                                                                                                                      |
|                  | Advanced > Extension to Delete on Clean | Add *.pyd; into the list                                                                                                  |
| C/C++ > General  | Additional Include Directories          | Add Python `include` folder, e.g. `C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\include`             |
|                  |                                         | Add PyBind11 `include` folder, e.g. `C:\Users\mbazli\AppData\Roaming\Python\Python37\site-packages\pybind11\include`      |
| Linker > General | Additional Library Directories          | Add Python `libs` that contain *.lib files, e.g. `C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\libs` |